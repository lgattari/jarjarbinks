import Vue from 'vue';
import App from './App.vue';
import $ from 'jquery';
import vuetify from '@/plugins/vuetify'; // path to vuetify export
import VueResource from 'vue-resource';

Vue.config.productionTip = false;
Vue.use(VueResource);

Vue.mixin({
	methods: {
		showLoading: function (flag = true) {
			if (flag) $('#loadingOverlay').show();
			else $('#loadingOverlay').hide();
		}
	}
});
new Vue({
	render: h => h(App),
	vuetify
}).$mount('#app');
