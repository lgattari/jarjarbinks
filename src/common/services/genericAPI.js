import Vue from 'vue';
import axios from 'axios';

const msgError =
   'Error.';

export function GET (URL, showWarning = true) {
	// Vue.axios.defaults.headers.common.Authorization = authHeader().Authorization;

	return axios
		.get(URL)
		.then(function (response) {
			if (response.status !== 200) {
				throw {
					service: URL,
					msg: response.data.Message,
					status: response.status
				};
			} else {
				if (
					response.data === null ||
               response.data === undefined
				) {
					if (showWarning) console.log('No records');
				}
				return Promise.resolve(response.data);
			}
		})
		.catch(error => {
			return errorHandler(error.response);
		});
}

const errorHandler = function (err) {
	Vue.prototype.$toastr.defaultPreventDuplicates = true;
	if (err) {
		switch (err.status) {
		case 400:
			if (
				err.data.Message !== null &&
               err.data.Message !== undefined &&
               err.data.Message !== ''
			) { Vue.prototype.$toastr.e(err.data.Message); }
			break;
		case 401:
			if (
				err.data.Message !== null &&
               err.data.Message !== undefined &&
               err.data.Message !== ''
			) { Vue.prototype.$toastr.e(err.data.Message); }
			break;
		default:
			if (
				err.data.Message !== null &&
               err.data.Message !== undefined &&
               err.data.Message !== ''
			) { Vue.prototype.$toastr.e(err.data.Message); }
			break;
		}
	} else {
		Vue.prototype.$toastr.e(msgError);
	}
	return Promise.reject(err);
};
